package com.plux.web.example;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.xml.XmlConfiguration;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Jetty实现简单文件服务器(基于xml配置文件)
 * Created by idist on 2017/4/16.
 */
public class JettyResourceServerExample {

    public static void main(String[] args) throws Exception {
       // System.out.println(System.getProperty("user.dir"));
        String fileName = "jettyResourceServer.xml";
        Server jettyServer = null;
        try{
            String xmlFilePath = initResourceFilePath(fileName);
            jettyServer = InitJettyWebServer(xmlFilePath);
            startJettyResourceServer(jettyServer);
        } catch(Exception ex){
            if (jettyServer != null) {
                // 触发异常则关闭Server
                jettyServer.destroy();
            }
            System.err.println("start JettyResourceServerExample error. msg=" + ex.getMessage());
            ex.printStackTrace();
        }
    }



    private static Server InitJettyWebServer(String xmlFilePath) throws Exception {
        // 调用org.eclipse.jetty.util.resource.Resource.newClassPathResource()读取不到xmlFilePath的内容
        // Resource resource = Resource.newClassPathResource(xmlFilePath);
        File xmlFile = new File(xmlFilePath);
        XmlConfiguration configuration = new XmlConfiguration(new FileInputStream(xmlFile));
        Server jettyServer = (Server) configuration.configure();
        return jettyServer;
    }

    private static void startJettyResourceServer(Server jettyServer) throws Exception {
        try {
            jettyServer.start();
            jettyServer.join();
            System.out.println("jetty resource server start successful ... ");

            // 打印dump时的信息
            // System.out.println(jettyServer.dump());
        } catch (Exception ex) {
            String msg = ex.getMessage();
            System.err.println("jetty resource server start error, msg=" + msg);
            throw ex;
        }
    }

    private static String initResourceFilePath(String fileName) throws IOException, SAXException {
        //String basePath = JettyResourceServerExample.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        // 用户的当前工作目录
        String basePath = System.getProperty("user.dir") + File.separator + fileName;
        System.out.println("basePath=" + basePath);
        File xmlFile = new File(fileName);
        return xmlFile.getAbsolutePath();
    }
}
