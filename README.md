#jetty-server-example

1、执行assembly打包后，产生jetty-server-example-1.0.0-with-dependencies.jar包
```bash
mvn clean compile package install assembly:assembly
```

2、拷贝 start.sh、jettyResourceServer.xml，保持三个文件（start.sh、jettyResourceServer.xml、jetty-server-example-1.0.0-with-dependencies.jar）处于相同目录

3、执行start脚本即可    
```bash
sh start.sh
```

4、运行效果       
![Http访问示例图](https://git.oschina.net/uploads/images/2017/0416/192205_a7c050e4_859722.png "")
![本地磁盘资源文件](https://git.oschina.net/uploads/images/2017/0416/192220_bd59f260_859722.png "")
